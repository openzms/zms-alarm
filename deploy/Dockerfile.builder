FROM golang:1.22-bookworm as build

RUN apt-get update -y \
  && DEBIAN_FRONTEND=noninteractive \
    apt-get install -y \
    make git less nano ca-certificates \
    libsuitesparse-dev \
    liblpsolve55-dev \
  && apt-get clean all \
  && rm -rfv /var/lib/apt/lists/*

ENV CGO_CFLAGS="-I/usr/include/lpsolve"
ENV CGO_LDFLAGS="-llpsolve55 -lm -ldl -lcolamd"
