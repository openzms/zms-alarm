// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package config

import (
	"flag"
	"os"
	"strconv"
)

type Config struct {
	Verbose             bool
	Debug               bool
	HttpEndpointListen  string
	HttpEndpoint        string
	HealthHttpEndpoint  string
	RpcEndpointListen   string
	RpcEndpoint         string
	ServiceId           string
	ServiceName         string
	DbDriver            string
	DbDsn               string
	DbAutoMigrate       bool
	IdentityRpcEndpoint string
}

func getEnvBool(name string, defValue bool) bool {
	if val, ok := os.LookupEnv(name); ok {
		if pval, err := strconv.ParseBool(val); err != nil {
			pval = false
		} else {
			return pval
		}
	}
	return defValue
}

func getEnvString(name string, defValue string) string {
	if val, ok := os.LookupEnv(name); ok {
		return val
	}
	return defValue
}

func LoadConfig() *Config {
	var config Config
	flag.BoolVar(&config.Debug, "debug",
		getEnvBool("LOG_DEBUG", false), "Enable debug logging.")
	flag.BoolVar(&config.Verbose, "verbose",
		getEnvBool("LOG_VERBOSE", false), "Enable verbose logging.")

	flag.StringVar(&config.HttpEndpointListen, "http-endpoint-listen",
		getEnvString("HTTP_ENDPOINT_LISTEN", "0.0.0.0:8030"), "HTTP endpoint bind address")
	flag.StringVar(&config.HttpEndpoint, "http-endpoint",
		getEnvString("HTTP_ENDPOINT", "0.0.0.0:8030"), "HTTP endpoint address")
	flag.StringVar(&config.HealthHttpEndpoint, "health-http-endpoint",
		getEnvString("HEALTH_HTTP_ENDPOINT", "0.0.0.0:8031"), "HTTP health endpoint address")
	flag.StringVar(&config.RpcEndpointListen, "rpc-endpoint-listen",
		getEnvString("RPC_ENDPOINT_LISTEN", "0.0.0.0:8032"), "RPC endpoint bind address")
	flag.StringVar(&config.RpcEndpoint, "rpc-endpoint",
		getEnvString("RPC_ENDPOINT", "0.0.0.0:8032"), "RPC endpoint address")
	flag.StringVar(&config.ServiceId, "service-id",
		getEnvString("SERVICE_ID", "83525543-bd21-4e8b-87a6-c8f06a18898e"), "Service ID")
	flag.StringVar(&config.ServiceName, "service-name",
		getEnvString("SERVICE_NAME", "alarm"), "Service Name")
	flag.StringVar(&config.DbDriver, "db-driver",
		getEnvString("DB_DRIVER", "sqlite"), "Database driver (sqlite, postgres)")
	flag.StringVar(&config.DbDsn, "db-dsn",
		getEnvString("DB_DSN", "file::memory:?cache=shared"), "Database DSN")
	flag.BoolVar(&config.DbAutoMigrate, "db-auto-migrate",
		getEnvBool("DB_AUTO_MIGRATE", true), "Enable/disable automatic database migration")
	flag.StringVar(&config.IdentityRpcEndpoint, "identity-rpc-endpoint",
		getEnvString("IDENTITY_RPC_ENDPOINT", "0.0.0.0:8002"), "Identity service RPC endpoint address")

	flag.Parse()

	return &config
}
