// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package store

import (
	"google.golang.org/protobuf/types/known/timestamppb"

	alarm_v1 "gitlab.flux.utah.edu/openzms/zms-api/go/zms/alarm/v1"
)

// NB: caller must Preload any relations; elaborate parameter only controls
// output of them, to give caller more flexibility.

func (in *ConstraintChange) ToProto(out *alarm_v1.ConstraintChange, elaborate bool) error {
	out.Id = in.Id.String()
	out.GrantId = in.GrantId.String()
	out.ConstraintId = in.ConstraintId.String()
	if in.ObservationId != nil {
		s := in.ObservationId.String()
		out.ObservationId = &s
	}
	out.MinFreq = in.MinFreq
	out.MaxFreq = in.MaxFreq
	out.Bandwidth = in.Bandwidth
	out.MinEirp = in.MinEirp
	out.MaxEirp = in.MaxEirp
	out.Description = in.Description
	if in.CreatorId != nil {
		s := in.CreatorId.String()
		out.CreatorId = &s
	}
	if in.UpdaterId != nil {
		s := in.UpdaterId.String()
		out.UpdaterId = &s
	}
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.UpdatedAt != nil {
		out.UpdatedAt = timestamppb.New(*in.UpdatedAt)
	}
	if in.DeletedAt != nil {
		out.DeletedAt = timestamppb.New(*in.DeletedAt)
	}
	return nil
}

func (in *GrantChange) ToProto(out *alarm_v1.GrantChange, elaborate bool) error {
	out.Id = in.Id.String()
	out.GrantId = in.GrantId.String()
	if in.ObservationId != nil {
		s := in.ObservationId.String()
		out.ObservationId = &s
	}
	out.Action = in.Action
	out.Description = in.Description
	if in.CreatorId != nil {
		s := in.CreatorId.String()
		out.CreatorId = &s
	}
	if in.UpdaterId != nil {
		s := in.UpdaterId.String()
		out.UpdaterId = &s
	}
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.UpdatedAt != nil {
		out.UpdatedAt = timestamppb.New(*in.UpdatedAt)
	}
	if in.DeletedAt != nil {
		out.DeletedAt = timestamppb.New(*in.DeletedAt)
	}
	return nil
}
