// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package store

import (
	"errors"
	"fmt"
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

type ConstraintChange struct {
	Id            uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	GrantId       uuid.UUID  `gorm:"type:uuid" json:"grant_id" binding:"required"`
	ConstraintId  uuid.UUID  `gorm:"type:uuid" json:"constraint_id" binding:"required"`
	ObservationId *uuid.UUID `gorm:"type:uuid" json:"observation_id"`
	MinFreq       *int64     `json:"min_freq"`
	MaxFreq       *int64     `json:"max_freq"`
	Bandwidth     *int64     `json:"bandwidth"`
	MaxEirp       *float64   `json:"max_eirp"`
	MinEirp       *float64   `json:"min_eirp"`
	Description   *string    `json:"description"`
	CreatorId     *uuid.UUID `gorm:"type:uuid" json:"creator_id"`
	UpdaterId     *uuid.UUID `gorm:"type:uuid" json:"updater_id"`
	CreatedAt     time.Time  `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt     *time.Time `gorm:"autoUpdateTime" json:"updated_at"`
	DeletedAt     *time.Time `json:"deleted_at"`
}

func (x *ConstraintChange) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type GrantChange struct {
	Id            uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	GrantId       uuid.UUID  `gorm:"type:uuid" json:"grant_id" binding:"required"`
	ObservationId *uuid.UUID `gorm:"type:uuid" json:"observation_id"`
	Action        *string    `json:"action"`
	Description   *string    `json:"description"`
	CreatorId     *uuid.UUID `gorm:"type:uuid" json:"creator_id"`
	UpdaterId     *uuid.UUID `gorm:"type:uuid" json:"updater_id"`
	CreatedAt     time.Time  `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt     *time.Time `gorm:"autoUpdateTime" json:"updated_at"`
	DeletedAt     *time.Time `json:"deleted_at"`
}

func (x *GrantChange) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Comparator string

const (
	CompNotEqual           Comparator = "ne"
	CompEqual              Comparator = "eq"
	CompGreaterThan        Comparator = "gt"
	CompLessThan           Comparator = "lt"
	CompGreaterThanOrEqual Comparator = "gte"
	CompLessThanOrEqual    Comparator = "lte"
)

// NB: ensure this matches the above enumerated string type.
const comparatorCreatePostgres string = "CREATE TYPE comparator_enum AS ENUM ('ne', 'eq', 'gt', 'lt', 'gte', 'lte')"

func (c *Comparator) Scan(value interface{}) error {
	s, ok := value.(string)
	if !ok {
		return errors.New("invalid Comparator value")
	}
	*c = Comparator(s)
	return nil
}

func (c Comparator) Value() (interface{}, error) {
	return string(c), nil
}

func (Comparator) GormDBDataType(db *gorm.DB, field *schema.Field) string {
	switch db.Dialector.Name() {
	case "sqlite":
		return "text"
	case "postgres":
		return "comparator_enum"
	}
	return ""
}

func CreateComparatorEnumPostgres(db *gorm.DB) (err error) {
	res := db.Debug().Exec(fmt.Sprintf(`
	DO $$ BEGIN
		%s;
	EXCEPTION
		WHEN duplicate_object THEN null;
	END $$;`, comparatorCreatePostgres))
	if res.Error != nil {
		return res.Error
	}
	return nil
}

type GrantAlarm struct {
	Id                uuid.UUID       `gorm:"primaryKey;type:uuid" json:"id"`
	GrantId           uuid.UUID       `gorm:"type:uuid" json:"grant_id" binding:"required"`
	ElementId         uuid.UUID       `gorm:"type:uuid" json:"element_id" binding:"required"`
	SpectrumId        uuid.UUID       `gorm:"type:uuid" json:"spectrum_id" binding:"required"`
	RtIntConstraintId *uuid.UUID      `gorm:"type:uuid" json:"rt_int_constraint_id" binding:"required"`
	Name              string          `gorm:"not null;size:256" json:"name" binding:"required"`
	Metric            string          `json:"metric" json:"name" binding:"required"`
	Comparator        Comparator      `gorm:"not null;default:'gt'" json:"comparator"`
	Threshold         float64         `json:"threshold" binding:"required"`
	Aggregator        *string         `json:"aggregator"`
	Period            *int            `json:"period"`
	Value             float64         `json:"value"`
	Alarming          *bool           `json:"alarming"`
	CurrentValue      float64         `json:"current_value"`
	CreatorId         *uuid.UUID      `gorm:"type:uuid" json:"creator_id"`
	UpdaterId         *uuid.UUID      `gorm:"type:uuid" json:"updater_id"`
	CreatedAt         time.Time       `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt         *time.Time      `gorm:"autoUpdateTime" json:"updated_at"`
	DeletedAt         *time.Time      `json:"deleted_at"`
	Logs              []GrantAlarmLog `json:"logs"`
}

func (x *GrantAlarm) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type GrantAlarmLog struct {
	Id           uuid.UUID `gorm:"primaryKey;type:uuid" json:"id"`
	GrantAlarmId uuid.UUID `gorm:"foreignKey:GrantAlarmId;type:uuid;not null" json:"grant_alarm_id"`
	Alarming     bool      `json:"alarming"`
	CurrentValue float64   `json:"current_value"`
	Message      string    `gorm:"not null;size:4096" json:"message"`
	CreatedAt    time.Time `gorm:"autoCreateTime" json:"created_at"`
}

func (x *GrantAlarmLog) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}



var CustomSqlPostgres []string = []string{
	comparatorCreatePostgres,
}

var AllTables []interface{} = []interface{}{
	&ConstraintChange{}, &GrantChange{}, &GrantAlarm{}, &GrantAlarmLog{},
}
