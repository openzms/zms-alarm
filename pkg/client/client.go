// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package client

import (
	"context"
	"errors"
	"fmt"
	"math"
	"strconv"
	"time"

	"github.com/draffensperger/golp"
	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc/codes"
	"gorm.io/gorm"

	zmsclient "gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/client"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/subscription"

	alarm "gitlab.flux.utah.edu/openzms/zms-api/go/zms/alarm/v1"
	dst "gitlab.flux.utah.edu/openzms/zms-api/go/zms/dst/v1"
	event "gitlab.flux.utah.edu/openzms/zms-api/go/zms/event/v1"
	geo "gitlab.flux.utah.edu/openzms/zms-api/go/zms/geo/v1"
	zmc "gitlab.flux.utah.edu/openzms/zms-api/go/zms/zmc/v1"

	"gitlab.flux.utah.edu/openzms/zms-alarm/pkg/config"
	"gitlab.flux.utah.edu/openzms/zms-alarm/pkg/store"
)

type ObservationWatcher struct {
	config  *config.Config
	db      *gorm.DB
	sm      *subscription.SubscriptionManager[*subscription.Event]
	rclient *zmsclient.ZmsClient
	sub     *zmsclient.Subscription[dst.SubscribeResponse]
}

func (ow *ObservationWatcher) StatusHandler(connected bool, err error) (abort bool) {
	if !connected {
		log.Warn().Msg(fmt.Sprintf("ObservationWatcher: disconnected (%+v)", err))
	} else {
		log.Info().Msg(fmt.Sprintf("ObservationWatcher: connected: %+v", connected))
	}

	return false
}

func (ow *ObservationWatcher) VerifiedViolationHandler(observation *dst.Observation) (code codes.Code, err error) {
	if observation.ViolationVerifiedAt == nil || observation.ViolatedGrantId == nil {
		return
	}

	log.Info().Msg(fmt.Sprintf("ObservationWatcher: received pre-verified violation of grant %s", *observation.ViolatedGrantId))

	var zmcClient zmc.ZmcClient
	if zmcClient, err = ow.rclient.GetZmcClient(context.Background()); err != nil {
		return codes.Internal, fmt.Errorf("Failed to obtain zmc service client")
	}

	// Verify grant uuid is still present at zmc.
	ff := false
	reqId := uuid.New().String()
	req := zmc.GetGrantRequest{
		Header: &zmc.RequestHeader{
			ReqId:     &reqId,
			Elaborate: &ff,
		},
		Id: *observation.ViolatedGrantId,
	}
	var grant *zmc.Grant
	var resp *zmc.GetGrantResponse
	if resp, err = zmcClient.GetGrant(context.Background(), &req); err != nil {
		return codes.Internal, fmt.Errorf("Failed to obtain grants from zmc service")
	} else if resp.Grant == nil {
		return codes.NotFound, fmt.Errorf("No such grant at zmc service")
	} else {
		grant = resp.Grant
	}

	var grantId uuid.UUID
	if idParsed, err := uuid.Parse(grant.Id); err != nil {
		return codes.InvalidArgument, err
	} else {
		grantId = idParsed
	}
	var observationId uuid.UUID
	if idParsed, err := uuid.Parse(observation.Id); err != nil {
		return codes.InvalidArgument, err
	} else {
		observationId = idParsed
	}

	t := time.Now().UTC()
	action := "revoke"
	grantChange := store.GrantChange{
		Id:            uuid.New(),
		GrantId:       grantId,
		ObservationId: &observationId,
		Action:        &action,
		CreatedAt:     t,
	}

	// Generate an event to recommend the revocation.
	oid := grantChange.Id.String()
	userIdStr := grant.CreatorId
	elementIdStr := grant.ElementId
	eh := subscription.EventHeader{
		Type:       int32(event.EventType_ET_ACTION),
		Code:       int32(alarm.EventCode_EC_GRANTCHANGE),
		SourceType: int32(event.EventSourceType_EST_ALARM),
		SourceId:   ow.config.ServiceId,
		Id:         uuid.New().String(),
		ObjectId:   &oid,
		Time:       &t,
		UserId:     &userIdStr,
		ElementId:  &elementIdStr,
	}
	e := subscription.Event{
		Header: eh,
		Object: &grantChange,
	}
	log.Debug().Msg(fmt.Sprintf("sending grant revocation event: %+v", e))
	go ow.sm.Notify(&e)

	return codes.OK, nil
}

func (ow *ObservationWatcher) GetInterferedGrant(grants []*zmc.Grant) (rxGrant *zmc.Grant, rxPort *zmc.RadioPort, err error) {
	// Get first grant with radio port rx only
	for _, grant := range grants {
		for _, radioPort := range grant.GetRadioPorts() {
			if !radioPort.Tx && radioPort.Rx {
				return grant, radioPort, nil
			}
		}
	}
	return nil, nil, fmt.Errorf("No rx only grants")
}

func (ow *ObservationWatcher) UpdateGrantAlarm(grantId string, alarming *bool, msg string) error {
	// Update Grant Alarm
	if res := ow.db.Model(&store.GrantAlarm{}).Where("grant_id = ?", grantId).Update("alarming", alarming); res.Error != nil {
		m := fmt.Sprintf("error handling alarming grant (%s): %s", grantId, res.Error.Error())
		log.Error().Err(res.Error).Msg(m)
		return res.Error
	}

	// Get grant alarm id
	var parsedGrantId *uuid.UUID
	if idParsed, err := uuid.Parse(grantId); err != nil {
		return errors.New("Invalid grant Id")
	} else {
		parsedGrantId = &idParsed
	}
	var grantAlarm store.GrantAlarm
	q := ow.db.Where(&store.GrantAlarm{GrantId: *parsedGrantId})
	res := q.First(&grantAlarm)
	if res.Error != nil || res.RowsAffected != 1 {
		return errors.New("Grant Alarm not found")
	}

	// Create grant alarm log
	gal := store.GrantAlarmLog{
		Id:           uuid.New(),
		GrantAlarmId: grantAlarm.Id,
		Alarming:     *alarming,
		Message:      fmt.Sprintf("revoke: %s", msg),
		CreatedAt:    time.Now().UTC(),
	}
	if res := ow.db.Create(&gal); res.Error != nil {
		log.Error().Err(res.Error).Msg(fmt.Sprintf("error creating GrantAlarmLog: %s", res.Error.Error()))
		return res.Error
	}
	return nil
}

func (ow *ObservationWatcher) GetLoss(dstClient dst.DstClient, secGrant *zmc.Grant, observation *dst.Observation, intLoc *zmc.Location) (loss float64, err error) {
	// Get expected loss from dst
	expLossReqId := uuid.New().String()
	tt := true
	var radioPortId string
	for _, radioPort := range secGrant.GetRadioPorts() {
		radioPortId = radioPort.Id
	}
	expLossReq := dst.GetExpectedLossRequest{
		Header: &dst.RequestHeader{
			ReqId:     &expLossReqId,
			Elaborate: &tt,
		},
		RadioPortId: radioPortId,
		Freq:        observation.MinFreq,
		Point: &geo.Point{
			Srid: intLoc.Srid,
			X:    intLoc.X,
			Y:    intLoc.Y,
			Z:    intLoc.Z,
		},
	}
	var expLossResp *dst.GetExpectedLossResponse
	if expLossResp, err = dstClient.GetExpectedLoss(context.Background(), &expLossReq); err != nil {
		return 0, err
	}
	return expLossResp.Loss, nil
}

func (ow *ObservationWatcher) OptimalAllocation(zmcClient zmc.ZmcClient, grants []*zmc.Grant, intGrant *zmc.Grant, intRadioPort *zmc.RadioPort, observation *dst.Observation) (code codes.Code, err error) {
	// Get dst client
	var dstClient dst.DstClient
	if dstClient, err = ow.rclient.GetDstClient(context.Background()); err != nil {
		return codes.Internal, fmt.Errorf("Failed to obtain dst service client")
	}

	// Get interfered radio's location
	locReqId := uuid.New().String()
	tt := true
	locReq := zmc.GetLocationRequest{
		Header: &zmc.RequestHeader{
			ReqId:     &locReqId,
			Elaborate: &tt,
		},
		Id: intRadioPort.GetAntennaLocationId(),
	}
	var locResp *zmc.GetLocationResponse
	if locResp, err = zmcClient.GetLocation(context.Background(), &locReq); err != nil {
		return codes.Internal, fmt.Errorf("Failed to obtain antenna location")
	}
	intLoc := locResp.GetLocation()

	// Get interference threshold
	var intThreshold float64
	for _, intConst := range intGrant.GetIntConstraints() {
		intThreshold = *intConst.MaxPower
	}

	// num of secondary (lower priority) grants
	secGrantsNum := len(grants) - 1 // Assumes only one primary grant

	// Get max gain for each secondary grant
	var pGain []float64
	var pLoss []float64
	var loss float64
	for _, grant := range grants {
		for _, radioPort := range grant.GetRadioPorts() { // Assumes one radio port per grant
			if radioPort.Tx && !radioPort.Rx {
				pGain = append(pGain, math.Pow(10, radioPort.MaxPower/10)) // Convert to linear scale
				if loss, err = ow.GetLoss(dstClient, grant, observation, intLoc); err != nil {
					return codes.Internal, err
				}
				pLoss = append(pLoss, math.Pow(10, loss/10)) // Linear pathloss
			}
		}
	}
	lp := golp.NewLP(0, 2*secGrantsNum)
	var pEntries []golp.Entry
	var pObj []float64
	var zObj []float64
	for i := 0; i < secGrantsNum; i++ {
		lp.AddConstraintSparse([]golp.Entry{{i, 1.0}}, golp.LE, pGain[i])                           // p <= pGain
		lp.AddConstraintSparse([]golp.Entry{{i, 1.0}, {i + secGrantsNum, -pGain[i]}}, golp.LE, 0.0) // p <= pGain * z
		lp.AddConstraintSparse([]golp.Entry{{i, -1.0}, {i + secGrantsNum, 1.0}}, golp.LE, 0.0)      // p >= z
		lp.SetBinary(i+secGrantsNum, true)                                                          // z is binary
		pEntries = append(pEntries, golp.Entry{i, pLoss[i] * 1e10})                                 // sum p * loss
		pObj = append(pObj, 0.01)
		zObj = append(zObj, 1.0)
	}
	lp.AddConstraintSparse(pEntries, golp.LE, math.Pow(10, intThreshold/10)*1e10)
	lp.SetObjFn(append(pObj, zObj...))
	lp.SetMaximize()
	lp.Solve()

	vars := lp.Variables()
	i := 0
	for _, grant := range grants {
		for _, radioPort := range grant.GetRadioPorts() {
			if radioPort.Tx && !radioPort.Rx {
				var grantId uuid.UUID
				if idParsed, err := uuid.Parse(grant.Id); err != nil {
					return codes.InvalidArgument, err
				} else {
					grantId = idParsed
				}
				var observationId uuid.UUID
				if idParsed, err := uuid.Parse(observation.Id); err != nil {
					return codes.InvalidArgument, err
				} else {
					observationId = idParsed
				}
				t := time.Now().UTC()
				if vars[i+secGrantsNum] == 1 {
					var constraintId uuid.UUID
					for _, constraint := range grant.GetConstraints() {
						if idParsed, err := uuid.Parse(constraint.Id); err != nil {
							return codes.InvalidArgument, err
						} else {
							constraintId = idParsed
						}
					}
					maxEirp := math.Round(10 * math.Log10(vars[i]))
					log.Debug().Msg(fmt.Sprintf("Change MaxEirp: %v", maxEirp))
					constraintChange := store.ConstraintChange{
						Id:            uuid.New(),
						GrantId:       grantId,
						ObservationId: &observationId,
						ConstraintId:  constraintId,
						MaxEirp:       &maxEirp,
						CreatedAt:     t,
					}
					oid := constraintChange.Id.String()
					userIdStr := grant.CreatorId
					elementIdStr := grant.ElementId
					eh := subscription.EventHeader{
						Type:       int32(event.EventType_ET_ACTION),
						Code:       int32(alarm.EventCode_EC_CONSTRAINTCHANGE),
						SourceType: int32(event.EventSourceType_EST_ALARM),
						SourceId:   ow.config.ServiceId,
						Id:         uuid.New().String(),
						ObjectId:   &oid,
						Time:       &t,
						UserId:     &userIdStr,
						ElementId:  &elementIdStr,
					}
					e := subscription.Event{
						Header: eh,
						Object: &constraintChange,
					}
					log.Debug().Msg(fmt.Sprintf("sending constraint change event: %+v", e))
					go ow.sm.Notify(&e)
				} else {
					action := "pause"
					grantChange := store.GrantChange{
						Id:            uuid.New(),
						GrantId:       grantId,
						ObservationId: &observationId,
						Action:        &action,
						CreatedAt:     t,
					}
					oid := grantChange.Id.String()
					userIdStr := grant.CreatorId
					elementIdStr := grant.ElementId
					eh := subscription.EventHeader{
						Type:       int32(event.EventType_ET_ACTION),
						Code:       int32(alarm.EventCode_EC_GRANTCHANGE),
						SourceType: int32(event.EventSourceType_EST_ALARM),
						SourceId:   ow.config.ServiceId,
						Id:         uuid.New().String(),
						ObjectId:   &oid,
						Time:       &t,
						UserId:     &userIdStr,
						ElementId:  &elementIdStr,
					}
					e := subscription.Event{
						Header: eh,
						Object: &grantChange,
					}
					log.Debug().Msg(fmt.Sprintf("sending grant change event: %+v", e))
					go ow.sm.Notify(&e)
				}
				i++
			}
		}
	}
	return codes.OK, nil
}

func (ow *ObservationWatcher) InterferenceHandler(observation *dst.Observation) (code codes.Code, err error) {
	// Get grant experiencing interference
	var zmcClient zmc.ZmcClient
	if zmcClient, err = ow.rclient.GetZmcClient(context.Background()); err != nil {
		return codes.Internal, fmt.Errorf("Failed to obtain zmc service client")
	}

	// Revisit how to associate OTA observations to grants for other cases
	ff := false
	tt := true
	reqId := uuid.New().String()
	req := zmc.GetGrantsRequest{
		Header: &zmc.RequestHeader{
			ReqId:     &reqId,
			Elaborate: &tt,
		},
		Freq:    &observation.MinFreq,
		Deleted: &ff,
		Denied:  &ff,
	}
	var resp *zmc.GetGrantsResponse
	if resp, err = zmcClient.GetGrants(context.Background(), &req); err != nil {
		return codes.Internal, fmt.Errorf("Failed to obtain grants from zmc service")
	} else if resp.Grants == nil {
		return codes.NotFound, fmt.Errorf("No grants at zmc service")
	} else {
		// Get grant with only rx set.
		var intGrant *zmc.Grant
		var intRadioPort *zmc.RadioPort
		if intGrant, intRadioPort, err = ow.GetInterferedGrant(resp.Grants); err != nil {
			return codes.Internal, err
		}

		var parsedIntGrantId *uuid.UUID
		if idParsed, err := uuid.Parse(intGrant.Id); err != nil {
			return codes.Internal, errors.New("Invalid grant Id")
		} else {
			parsedIntGrantId = &idParsed
		}

		// Get Grant Alarm
		var ga store.GrantAlarm
		q := ow.db.Where(&store.GrantAlarm{GrantId: *parsedIntGrantId})
		res := q.First(&ga)
		if res.Error != nil || res.RowsAffected != 1 {
			return codes.Internal, errors.New("Grant Alarm not found")
		}

		// Current interference period
		curPeriod := time.Unix(observation.EndsAt.GetSeconds(), int64(observation.EndsAt.GetNanos())).Sub(time.Unix(observation.StartsAt.GetSeconds(), int64(observation.StartsAt.GetNanos())))
		var intPeriod int
		if ga.Period != nil {
			intPeriod = *ga.Period + int(curPeriod.Seconds())
		} else {
			intPeriod = int(curPeriod.Seconds())
		}

		// Check if this is first interference notification
		//if ga.CurrentValue == 0 {
		if _, err = ow.OptimalAllocation(zmcClient, resp.Grants, intGrant, intRadioPort, observation); err != nil {
			return codes.Internal, err
		}
		//	return codes.OK, nil
		//}

		// Update period in grant alarm
		if res := ow.db.Model(&store.GrantAlarm{}).Where("grant_id = ?", intGrant.Id).Update("period", intPeriod); res.Error != nil {
			m := fmt.Sprintf("error handling alarming grant (%s): %s", intGrant.Id, res.Error.Error())
			log.Error().Err(res.Error).Msg(m)
			return codes.Internal, res.Error
		}

		totalTime := time.Now().Sub(time.Unix(intGrant.StartsAt.GetSeconds(), int64(intGrant.StartsAt.GetNanos())))
		log.Debug().Msg(fmt.Sprintf("cur val: %f", float64(intPeriod)/float64(totalTime.Seconds())))

		if float64(intPeriod)/float64(totalTime.Seconds()) >= ga.Threshold {
			curGrants := -1.0
			totalGrants := -1.0
			for _, grants := range resp.Grants {
				if grants.GetStatus() == "active" {
					curGrants += 1
				}
				totalGrants += 1
			}

			var revokeNum float64
			if curGrants > math.Round(ga.CurrentValue) {
				revokeNum = math.Round(ga.CurrentValue)
			} else {
				revokeNum = math.Round(totalGrants * (float64(intPeriod) / float64(totalTime.Seconds())) / ga.Threshold)
			}

			if revokeNum == 0 {
				revokeNum = 1
			}

			// Log grant alarm
			if err := ow.UpdateGrantAlarm(intGrant.Id, &tt, strconv.FormatFloat(revokeNum, 'f', -1, 64)); err != nil {
				return codes.Internal, err
			}

			// Get grants with lower priority (potential interferers)
			for _, grant := range resp.Grants {
				if grant.Priority < intGrant.Priority && grant.GetStatus() == "active" && revokeNum > 0 {
					revokeNum -= 1
					t := time.Now().UTC()
					action := "pause"
					var grantId uuid.UUID
					if idParsed, err := uuid.Parse(grant.Id); err != nil {
						return codes.InvalidArgument, err
					} else {
						grantId = idParsed
					}
					var observationId uuid.UUID
					if idParsed, err := uuid.Parse(observation.Id); err != nil {
						return codes.InvalidArgument, err
					} else {
						observationId = idParsed
					}
					grantChange := store.GrantChange{
						Id:            uuid.New(),
						GrantId:       grantId,
						ObservationId: &observationId,
						Action:        &action,
						CreatedAt:     t,
					}
					oid := grantChange.Id.String()
					userIdStr := grant.CreatorId
					elementIdStr := grant.ElementId
					eh := subscription.EventHeader{
						Type:       int32(event.EventType_ET_ACTION),
						Code:       int32(alarm.EventCode_EC_GRANTCHANGE),
						SourceType: int32(event.EventSourceType_EST_ALARM),
						SourceId:   ow.config.ServiceId,
						Id:         uuid.New().String(),
						ObjectId:   &oid,
						Time:       &t,
						UserId:     &userIdStr,
						ElementId:  &elementIdStr,
					}
					e := subscription.Event{
						Header: eh,
						Object: &grantChange,
					}
					go ow.sm.Notify(&e)
				}
			}
		} else {
			log.Debug().Msg("Interference below threshold!")
		}
	}
	return codes.OK, nil
}

func (ow *ObservationWatcher) ObservationHandler(observation *dst.Observation) (code codes.Code, err error) {
	var zmcClient zmc.ZmcClient
	if zmcClient, err = ow.rclient.GetZmcClient(context.Background()); err != nil {
		return codes.Internal, fmt.Errorf("Failed to obtain zmc service client")
	}
	ff := false
	tt := true
	reqId := uuid.New().String()
	req := zmc.GetGrantsRequest{
		Header: &zmc.RequestHeader{
			ReqId:     &reqId,
			Elaborate: &tt,
		},
		Freq:    &observation.MinFreq,
		Deleted: &ff,
		Denied:  &ff,
		Revoked: &ff,
	}

	var resp *zmc.GetGrantsResponse
	if resp, err = zmcClient.GetGrants(context.Background(), &req); err != nil {
		return codes.Internal, fmt.Errorf("Failed to obtain grants from zmc service")
	} else if resp.Grants == nil {
		return codes.NotFound, fmt.Errorf("No grants at zmc service")
	} else {
		// Get rx only grant
		var iGrant *zmc.Grant
		if iGrant, _, err = ow.GetInterferedGrant(resp.Grants); err != nil {
			return codes.Internal, err
		}

		var parsedIGrantId *uuid.UUID
		if idParsed, err := uuid.Parse(iGrant.Id); err != nil {
			return codes.Internal, errors.New("Invalid grant Id")
		} else {
			parsedIGrantId = &idParsed
		}

		// Get Grant Alarm
		var ga store.GrantAlarm
		q := ow.db.Where(&store.GrantAlarm{GrantId: *parsedIGrantId})
		res := q.First(&ga)
		if res.Error != nil || res.RowsAffected != 1 {
			return codes.Internal, errors.New("Grant Alarm not found")
		}

		// Current interference period
		var intPeriod int
		if ga.Period != nil {
			intPeriod = *ga.Period
		} else {
			intPeriod = 0
		}

		pausedNum := 0.0
		totalGrants := -1.0 // total number of grants in the same frequency range except the primary (rx only) grant
		for _, grant := range resp.Grants {
			if grant.GetStatus() == "paused" {
				pausedNum += 1
			}
			totalGrants += 1
		}

		var ewma float64
		ewmaAlpha := 0.125
		if ga.CurrentValue != 0 {
			ewma = (1-ewmaAlpha)*ga.CurrentValue + ewmaAlpha*pausedNum
		} else {
			ewma = pausedNum
		}

		log.Debug().Msg(fmt.Sprintf("EWMA: %v", ewma))

		if res := ow.db.Model(&store.GrantAlarm{}).Where("grant_id = ?", iGrant.Id).Update("current_value", ewma); res.Error != nil {
			m := fmt.Sprintf("error handling alarming grant (%s): %s", iGrant.Id, res.Error.Error())
			log.Error().Err(res.Error).Msg(m)
			return codes.Internal, res.Error
		}

		totalTime := time.Now().Sub(time.Unix(iGrant.StartsAt.GetSeconds(), int64(iGrant.StartsAt.GetNanos())))
		if float64(intPeriod)/float64(totalTime.Seconds()) < ga.Threshold {

			pausedNum = math.Round(totalGrants * (1 - (float64(intPeriod)/float64(totalTime.Seconds()))/ga.Threshold))

			// Log grant alarm
			if err := ow.UpdateGrantAlarm(iGrant.Id, &ff, strconv.FormatFloat(pausedNum, 'f', -1, 64)); err != nil {
				return codes.Internal, err
			}
			for _, grant := range resp.Grants {
				if grant.GetStatus() == "paused" && pausedNum > 0 {

					pausedNum -= 1

					// Update GrantAlarm.Value to store history

					t := time.Now().UTC()
					action := "resume"
					var grantId uuid.UUID
					if idParsed, err := uuid.Parse(grant.Id); err != nil {
						return codes.InvalidArgument, err
					} else {
						grantId = idParsed
					}
					var observationId uuid.UUID
					if idParsed, err := uuid.Parse(observation.Id); err != nil {
						return codes.InvalidArgument, err
					} else {
						observationId = idParsed
					}
					grantChange := store.GrantChange{
						Id:            uuid.New(),
						GrantId:       grantId,
						ObservationId: &observationId,
						Action:        &action,
						CreatedAt:     t,
					}
					oid := grantChange.Id.String()
					userIdStr := grant.CreatorId
					elementIdStr := grant.ElementId
					eh := subscription.EventHeader{
						Type:       int32(event.EventType_ET_ACTION),
						Code:       int32(alarm.EventCode_EC_GRANTCHANGE),
						SourceType: int32(event.EventSourceType_EST_ALARM),
						SourceId:   ow.config.ServiceId,
						Id:         uuid.New().String(),
						ObjectId:   &oid,
						Time:       &t,
						UserId:     &userIdStr,
						ElementId:  &elementIdStr,
					}
					e := subscription.Event{
						Header: eh,
						Object: &grantChange,
					}
					go ow.sm.Notify(&e)
				}
			}
		} else {
			log.Debug().Msg("Above interference threshold")
		}
	}
	return codes.OK, nil
}

func (ow *ObservationWatcher) MessageHandler(resp *dst.SubscribeResponse) (abort bool) {
	if resp.Events == nil {
		return
	}

	for _, e := range resp.Events {
		if e.Header == nil {
			continue
		}

		switch e.Header.Code {
		case int32(dst.EventCode_EC_OBSERVATION):
			if e.Header.Type == int32(event.EventType_ET_VIOLATION) {
				observation := e.GetObservation()
				log.Debug().Msg(fmt.Sprintf("ObservationWatcher: %+v", observation))

				if observation != nil && observation.ViolationVerifiedAt != nil && observation.ViolatedGrantId != nil {
					if code, err := ow.VerifiedViolationHandler(observation); err != nil {
						log.Error().Err(err).Msg(fmt.Sprintf("ObservationWatcher: error while handling: %s (%v, %+v)", err.Error(), code, observation))
					}
				}
			} else if e.Header.Type == int32(event.EventType_ET_INTERFERENCE) {
				observation := e.GetObservation()
				if code, err := ow.InterferenceHandler(observation); err != nil {
					log.Error().Err(err).Msg(fmt.Sprintf("ObservationWatcher: error while handling: %s (%v, %+v)", err.Error(), code, observation))
				}
			} else {
				observation := e.GetObservation()
				if code, err := ow.ObservationHandler(observation); err != nil {
					log.Error().Err(err).Msg(fmt.Sprintf("ObservationWatcher: error while handling: %s (%v, %+v)", err.Error(), code, observation))
				}
			}
		default:
			log.Debug().Msg(fmt.Sprintf("ObservationWatcher: unhandled msg %+v", e))
		}
	}

	return false
}

func NewObservationWatcher(ctx context.Context, config *config.Config, db *gorm.DB, rclient *zmsclient.ZmsClient, sm *subscription.SubscriptionManager[*subscription.Event]) (ow *ObservationWatcher) {
	filters := make([]*event.EventFilter, 0, 1)
	f := &event.EventFilter{
		Types: []int32{int32(event.EventType_ET_VIOLATION), int32(event.EventType_ET_INTERFERENCE), int32(event.EventType_ET_CREATED)},
		Codes: []int32{int32(dst.EventCode_EC_OBSERVATION)},
	}
	filters = append(filters, f)

	ow = &ObservationWatcher{
		config:  config,
		db:      db,
		sm:      sm,
		rclient: rclient,
	}

	t := true
	sub := rclient.NewDstSubscription(
		ow.MessageHandler, ow.StatusHandler, filters, &t, &t)
	ow.sub = sub

	sub.Run(ctx)

	return ow
}

type GrantWatcher struct {
	config  *config.Config
	db      *gorm.DB
	sm      *subscription.SubscriptionManager[*subscription.Event]
	rclient *zmsclient.ZmsClient
	sub     *zmsclient.Subscription[zmc.SubscribeResponse]
}

func (gw *GrantWatcher) StatusHandler(connected bool, err error) (abort bool) {
	if !connected {
		log.Warn().Msg(fmt.Sprintf("GrantWatcher: disconnected (%+v)", err))
	} else {
		log.Info().Msg(fmt.Sprintf("GrantWatcher: connected: %+v", connected))
	}

	return false
}

func (gw *GrantWatcher) GrantApprovedHandler(grant *zmc.Grant) (code codes.Code, err error) {
	for _, c := range grant.RtIntConstraints {
		var rtIntConstraintId uuid.UUID
		if idParsed, err := uuid.Parse(c.Id); err != nil {
			return codes.InvalidArgument, err
		} else {
			rtIntConstraintId = idParsed
		}
		var grantId uuid.UUID
		if idParsed, err := uuid.Parse(grant.Id); err != nil {
			return codes.InvalidArgument, err
		} else {
			grantId = idParsed
		}
		var elementId uuid.UUID
		if idParsed, err := uuid.Parse(grant.ElementId); err != nil {
			return codes.InvalidArgument, err
		} else {
			elementId = idParsed
		}
		var spectrumId uuid.UUID
		if idParsed, err := uuid.Parse(grant.ElementId); err != nil {
			return codes.InvalidArgument, err
		} else {
			spectrumId = idParsed
		}
		var creatorId uuid.UUID
		if idParsed, err := uuid.Parse(grant.CreatorId); err != nil {
			return codes.InvalidArgument, err
		} else {
			creatorId = idParsed
		}
		ff := false
		var period *int
		if c.Period != nil {
			p := int(*c.Period)
			period = &p
		}
		ga := store.GrantAlarm{
			Id:                uuid.New(),
			GrantId:           grantId,
			ElementId:         elementId,
			SpectrumId:        spectrumId,
			RtIntConstraintId: &rtIntConstraintId,
			Name:              c.Name + "-alarm",
			Metric:            c.Metric,
			Comparator:        store.Comparator(c.Comparator),
			Threshold:         c.Value,
			Aggregator:        c.Aggregator,
			Period:            period,
			Alarming:          &ff,
			CreatorId:         &creatorId,
			CreatedAt:         time.Now().UTC(),
		}
		if res := gw.db.Create(&ga); res.Error != nil {
			return codes.Internal, res.Error
		}
	}

	return codes.OK, nil
}

func (gw *GrantWatcher) GrantDeletedHandler(grant *zmc.Grant) (code codes.Code, err error) {
	t := time.Now().UTC()
	if res := gw.db.Model(&store.GrantAlarm{}).Where("grant_id = ?", grant.Id).Update("deleted_at", &t); res.Error != nil {
		m := fmt.Sprintf("error handling deleted grant (%s): %s", grant.Id, res.Error.Error())
		log.Error().Err(res.Error).Msg(m)
		return codes.Internal, errors.New(m)
	}

	return codes.OK, nil
}

func (gw *GrantWatcher) MessageHandler(resp *zmc.SubscribeResponse) (abort bool) {
	if resp.Events == nil {
		return
	}

	for _, e := range resp.Events {
		if e.Header == nil {
			continue
		}

		switch e.Header.Code {
		case int32(zmc.EventCode_EC_GRANT):
			// We start tracking grant.RtIntConstraints when the grant is
			// approved, and set our state to deleted when the grant is
			// deleted.
			if e.Header.Type == int32(event.EventType_ET_APPROVED) {
				grant := e.GetGrant()
				log.Debug().Msg(fmt.Sprintf("GrantWatcher: %+v approved", grant))
				if code, err := gw.GrantApprovedHandler(grant); err != nil {
					m := fmt.Sprintf("error handling approved grant %s: %s (%v)", grant.Id, err.Error(), code)
					log.Error().Msg(m)
				}
			} else if e.Header.Type == int32(event.EventType_ET_DELETED) {
				grant := e.GetGrant()
				log.Debug().Msg(fmt.Sprintf("GrantWatcher: %+v deleted", grant))
				if code, err := gw.GrantDeletedHandler(grant); err != nil {
					m := fmt.Sprintf("error handling deleted grant %s: %s (%v)", grant.Id, err.Error(), code)
					log.Error().Msg(m)
				}
			}
		default:
			log.Debug().Msg(fmt.Sprintf("GrantWatcher: unhandled msg %+v", e))
		}
	}

	return false
}

func NewGrantWatcher(ctx context.Context, config *config.Config, db *gorm.DB, rclient *zmsclient.ZmsClient, sm *subscription.SubscriptionManager[*subscription.Event]) (gw *GrantWatcher) {
	filters := make([]*event.EventFilter, 0, 1)
	f := &event.EventFilter{
		Codes: []int32{int32(zmc.EventCode_EC_GRANT)},
	}
	filters = append(filters, f)

	gw = &GrantWatcher{
		config:  config,
		db:      db,
		sm:      sm,
		rclient: rclient,
	}

	t := true
	sub := rclient.NewZmcSubscription(
		gw.MessageHandler, gw.StatusHandler, filters, &t, &t)
	gw.sub = sub

	sub.Run(ctx)

	return gw
}
