// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package northbound

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"

	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/client"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/policy"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/subscription"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/token"

	identity "gitlab.flux.utah.edu/openzms/zms-api/go/zms/identity/v1"

	"gitlab.flux.utah.edu/openzms/zms-alarm/pkg/version"
	"gitlab.flux.utah.edu/openzms/zms-alarm/pkg/store"
)

func (s *Server) SetupHealthRoutes() error {
	s.ginHealth.GET("/health/alive", s.GetHealthAlive)
	s.ginHealth.GET("/health/ready", s.GetHealthReady)
	return nil
}

func (s *Server) GetHealthAlive(c *gin.Context) {
	c.Status(http.StatusOK)
}

func (s *Server) GetHealthReady(c *gin.Context) {
	if db, err := s.db.DB(); err == nil {
		if err := db.Ping(); err == nil {
			c.Status(http.StatusOK)
			return
		}
	}
	c.Status(http.StatusServiceUnavailable)
}

func CheckElaborateMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		if tokenString := c.GetHeader("X-Api-Elaborate"); tokenString == "true" {
			c.Set("zms.elaborate", true)
		} else {
			c.Set("zms.elaborate", false)
		}
	}
}

func CheckForceUpdateMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		if tokenString := c.GetHeader("X-Api-Force-Update"); tokenString == "true" {
			c.Set("zms.force-update", true)
		} else {
			c.Set("zms.force-update", false)
		}
	}
}

func CheckTokenValue(s *Server, tokenString string) (*client.CachedToken, int, error) {
	if err := token.Validate(tokenString); err != nil {
		return nil, http.StatusBadRequest, fmt.Errorf("invalid token")
	}

	if tok, err := s.rclient.LookupToken(tokenString); tok != nil && err == nil {
		return tok, http.StatusOK, nil
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	var identityClient identity.IdentityClient
	var terr error
	if identityClient, terr = s.rclient.GetIdentityClient(ctx); terr != nil || identityClient == nil {
		return nil, http.StatusBadRequest, fmt.Errorf("identity service unavailable")
	}

	ctx, cancel = context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	tt := true
	req := identity.GetTokenRequest{
		Header: &identity.RequestHeader{
			Elaborate: &tt,
		},
		Token: tokenString,
	}
	resp, err := identityClient.GetToken(ctx, &req)
	if err != nil {
		rpcErr, _ := status.FromError(err)
		if rpcErr.Code() == codes.NotFound {
			return nil, http.StatusNotFound, fmt.Errorf("token not found")
		} else if rpcErr.Code() == codes.Unauthenticated {
			return nil, http.StatusUnauthorized, fmt.Errorf(rpcErr.Message())
		} else {
			return nil, http.StatusInternalServerError, fmt.Errorf("identity service error: %v", rpcErr.Message())
		}
	}
	if tok, err := s.rclient.CheckAndCacheToken(resp.Token); err != nil {
		return nil, http.StatusForbidden, fmt.Errorf("invalid token: %s", err.Error())
	} else {
		return tok, http.StatusOK, nil
	}
}

func CheckTokenMiddleware(s *Server) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		tokenString := c.GetHeader("X-Api-Token")
		if tokenString == "" {
			c.Set("zms.token", nil)
			return
		}
		var tok *client.CachedToken
		var code int
		if tok, code, err = CheckTokenValue(s, tokenString); err != nil {
			c.AbortWithStatusJSON(code, gin.H{"error": err.Error()})
			return
		} else {
			c.Set("zms.token", tok)
		}
	}
}

func (s *Server) CheckPolicyMiddleware(t *client.CachedToken, targetUserId *uuid.UUID, targetElementId *uuid.UUID, policies []policy.Policy) (bool, string) {
	log.Debug().Msg(fmt.Sprintf("token: %+v", t))
	for _, p := range policies {
		log.Debug().Msg(fmt.Sprintf("policy: %s", p.Name))
		for _, trb := range t.RoleBindings {
			if match := p.Check(trb.Role.Value, &t.UserId, trb.ElementId, targetUserId, targetElementId); match == true {
				log.Debug().Msg(fmt.Sprintf("policy match: %s", p.Name))
				return true, p.Name
			} else {
				log.Debug().Msg(fmt.Sprintf("policy mismatch: %s", p.Name))
			}
		}
	}
	return false, ""
}

func (s *Server) RequireContextToken(c *gin.Context) (*client.CachedToken, error) {
	value, exists := c.Get("zms.token")
	if !exists || value == nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "missing X-Api-Token"})
		return nil, errors.New("missing X-Api-Token")
	}
	return value.(*client.CachedToken), nil
}

func CorsMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Next()
	}
}

func (s *Server) SetupRoutes() error {
	v1 := s.gin.Group("/v1")

	v1.Use(CorsMiddleware())
	v1.Use(CheckElaborateMiddleware())
	v1.Use(CheckForceUpdateMiddleware())
	v1.Use(CheckTokenMiddleware(s))

	v1.GET("/version", s.GetVersion)

	v1.GET("/alarms/grants", s.GetGrantAlarmList)
	v1.GET("/alarms/grants/:grant_alarm_id", s.GetGrantAlarm)

	v1.GET("/subscriptions", s.GetSubscriptions)
	v1.POST("/subscriptions", s.CreateSubscription)
	v1.DELETE("/subscriptions/:subscription_id", s.DeleteSubscription)
	v1.GET("/subscriptions/:subscription_id/events", s.GetSubscriptionEvents)

	return nil
}

func GetPaginateParams(c *gin.Context) (int, int) {
	page, _ := strconv.Atoi(c.Query("page"))
	if page < 1 {
		page = 1
	}
	itemsPerPage, _ := strconv.Atoi(c.Query("items_per_page"))
	if itemsPerPage < 10 {
		itemsPerPage = 10
	} else if itemsPerPage > 100 {
		itemsPerPage = 100
	}
	return page, itemsPerPage
}

func Paginate(page int, itemsPerPage int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		offset := (page - 1) * itemsPerPage
		return db.Offset(offset).Limit(itemsPerPage)
	}
}

func (s *Server) GetVersion(c *gin.Context) {
	if _, err := s.RequireContextToken(c); err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	c.JSON(http.StatusOK, version.GetVersion())
}

type GetGrantAlarmListQueryParams struct {
	//Pending gin support...
	//ElementId *string `form:"element_id" binding:"omitempty,uuid"`
	Name          *string `form:"name" binding:"omitempty"`
	Alarming      *bool   `form:"alarming" binding:"omitempty"`
	Deleted       *bool   `form:"deleted" binding:"omitempty"`
	//SpectrumId *string `form:"spectrum_id" binding:"omitempty,uuid"`
	Sort    *string `form:"sort" binding:"omitempty,oneof=grant_id element_id created_at updated_at alarming deleted_at spectrum_id"`
	SortAsc *bool   `form:"sort_asc" binding:"omitempty"`
}

func (s *Server) GetGrantAlarmList(c *gin.Context) {
	var err error

	// Check optional filter query parameters.
	params := GetGrantAlarmListQueryParams{}
	if err = c.ShouldBindQuery(&params); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// Unmarshal query uuids.
	var elementId *uuid.UUID
	if p, exists := c.GetQuery("element_id"); exists {
		if idParsed, err := uuid.Parse(p); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid element_id uuid"})
			return
		} else {
			elementId = &idParsed
		}
	}
	var spectrumId *uuid.UUID
	if p, exists := c.GetQuery("spectrum_id"); exists {
		if idParsed, err := uuid.Parse(p); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid spectrum_id uuid"})
			return
		} else {
			spectrumId = &idParsed
		}
	}

	page, itemsPerPage := GetPaginateParams(c)

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	// Check policy.
	// XXX: really want to return unapproved grants as well
	// for callers with element roles, even if they do not specify
	// element_id.  Just need to apply the policy check to each element_id
	// in the token, and then allow grants matching those element_ids to be
	// unapproved or approved.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, elementId, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Admins can filter for anything; Viewers (e.g. if they have no element
	// binding) cannot filter; other users who have roles in Elements can
	// filter within those Elements.
	var elementIdList []*uuid.UUID
	if elementId != nil {
		elementIdList = []*uuid.UUID{elementId}
	} else if policyName != policy.MatchAdmin {
		elementIdList = tok.GetElementIds()
	}

	var grantAlarmList []store.GrantAlarm
	q := s.db
	if spectrumId != nil {
		q = q.Where("Grants.spectrum_id = ?", *spectrumId)
	}
	if len(elementIdList) > 0 {
		q = q.Where("Grants.element_id in ?", elementIdList)
	}
	if params.Alarming != nil {
		if !*params.Alarming {
			q = q.Where("grant_alarms.alarming is false")
		} else {
			q = q.Where("grant_alarms.alarming is true")
		}
	}
	if params.Deleted == nil || !*params.Deleted {
		q = q.Where("grant_alarms.deleted_at is NULL")
	} else {
		q = q.Where("grant_alarms.deleted_at is not NULL")
	}
	if params.Name != nil {
		q = q.Where(store.MakeILikeClause(s.config, "grant_alarms.name"), fmt.Sprintf("%%%s%%", *params.Name))
	}
	sortDir := "desc"
	if params.SortAsc != nil && *params.SortAsc {
		sortDir = "asc"
	}
	if params.Sort != nil {
		q = q.Order("grant_alarms." + *params.Sort + " " + sortDir)
	} else {
		q = q.Order("grant_alarms.updated_at " + sortDir)
	}

	var total int64
	cres := q.Model(&store.GrantAlarm{}).Count(&total)
	if cres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}

	if c.GetBool("zms.elaborate") {
		q = q.Preload("Logs")
	}

	qres := q.Scopes(store.Paginate(page, itemsPerPage)).Find(&grantAlarmList)
	if qres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"grant_alarms": grantAlarmList, "page": page, "total": total, "pages": int(math.Ceil(float64(total) / float64(itemsPerPage)))})
}

func (s *Server) GetGrantAlarm(c *gin.Context) {
	var err error

	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("grant_alarm_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	if match, _ = s.CheckPolicyMiddleware(tok, nil, nil, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	var grantAlarm store.GrantAlarm
	q := s.db
	if c.GetBool("zms.elaborate") {
		q = q.Preload("Logs")
	}
	res := q.First(&grantAlarm, id)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	c.JSON(http.StatusOK, grantAlarm)
}

type CreateSubscriptionModel struct {
	Id      string                     `json:"id" binding:"required,uuid"`
	Filters []subscription.EventFilter `json:"filters" binding:"omitempty"`
}

func (s *Server) CreateSubscription(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm CreateSubscriptionModel
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// NB: for now, non-admins will be restricted to self-user-associated
	// events.
	if match, _ := s.CheckPolicyMiddleware(tok, nil, nil, policy.AdminOnlyPolicy); !match {

		// Build a map of elementId string to (max) role value in that
		// element.
		// Also build a list of all ElementIds in the token.
		// Also build a list of ElementIds with role >= Operator.
		elementRoleMap := make(map[string]int)
		elementIds := make([]string, 0, len(tok.RoleBindings))
		opElementIds := make([]string, 0)
		for _, rb := range tok.RoleBindings {
			if rb.ElementId == nil || rb.Role == nil {
				continue
			}
			eis := rb.ElementId.String()
			nv := rb.Role.Value
			if v, ok := elementRoleMap[eis]; ok {
				if nv > v {
					elementRoleMap[eis] = nv
				}
			} else {
				elementRoleMap[eis] = nv
			}
			elementIds = append(elementIds, eis)
			if nv >= policy.RoleOperator {
				opElementIds = append(opElementIds, eis)
			}
		}
		userIds := []string{tok.UserId.String()}

		//
		// Each filter must match these constraint checks:
		//   * have UserIds set to exactly the calling user, and must
		//     have ElementIds set to a subset of the ElementIds in the token; OR
		//   * have UserIds set to exactly the calling user, and ElementIds nil; OR
		//   * have UserIds set to nil, and ElementIds set to elements in which the user has >= Operator role.
		//
		if dm.Filters == nil || len(dm.Filters) < 1 {
			if false {
				c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "non-admin user must provide at least one filter, with UserIds set to exactly their user ID, and ElementIds to a subset of the calling token's element IDs."})
				return
			} else {
				dm.Filters = []subscription.EventFilter{
					subscription.EventFilter{UserIds: userIds},
					subscription.EventFilter{UserIds: userIds, ElementIds: elementIds},
				}
				if len(opElementIds) > 0 {
					dm.Filters = append(dm.Filters, subscription.EventFilter{ElementIds: opElementIds})
				}
				log.Debug().Msg(fmt.Sprintf("autocreated user subscription %+v for token %+v", dm, tok))
			}
		}

		// Filters must be bound to calling user unless user has >= operator
		// role in the associated elements.
		for _, filter := range dm.Filters {
			if filter.UserIds == nil || len(filter.UserIds) == 0 {
				// In this case, the user must provide at least one element
				// in which they have >= Operator role.
				if len(opElementIds) == 0 {
					msg := "non-admin, non-operator token subscriptions must set UserIds to exactly their token UserId"
					c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
					return
				} else if filter.ElementIds == nil || len(filter.ElementIds) == 0 {
					msg := "token subscriptions without UserIds must set ElementIds to at least one Element in which they have >= Operator role."
					c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
					return
				} else {
					for _, eis := range filter.ElementIds {
						if rv, ok := elementRoleMap[eis]; !ok || rv < policy.RoleOperator {
							msg := fmt.Sprintf("non-admin, wildcard user filter cannot include a sub-Operator role (element %s)", eis)
							c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
							return
						}
					}
				}
			} else {
				if len(filter.UserIds) != 1 || filter.UserIds[0] != tok.UserId.String() {
					msg := "non-admin subscription must set each filter.UserIds field to a one-item list containing exactly the calling token UserId."
					c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
					return
				}
				if filter.ElementIds == nil || len(filter.ElementIds) < 1 {
					//msg := "non-admin subscription must set each filter.ElementIds field to a subset of the ElementIds in the calling token."
					//c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
					//return
				} else {
					for _, elementId := range filter.ElementIds {
						if _, ok := elementRoleMap[elementId]; !ok {
							msg := "non-admin user must set each filter's ElementIds field to a subset of the ElementIds in the calling token."
							c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
							return
						}
					}
				}
			}
		}
	}

	if err := s.sm.Subscribe(dm.Id, dm.Filters, nil, c.ClientIP(), subscription.Rest, &tok.Token); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.Status(http.StatusCreated)

	return
}

func (s *Server) GetSubscriptions(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	if match, _ := s.CheckPolicyMiddleware(tok, nil, nil, policy.AdminOnlyPolicy); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"subscriptions": s.sm.GetSubscriptions()})
	return
}

func (s *Server) GetSubscriptionEvents(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("subscription_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
	} else {
		id = idParsed
	}
	idStr := id.String()

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	var subprotos []string
	if xApiTok := c.GetHeader("X-Api-Token"); xApiTok != "" {
		if tok, err = s.RequireContextToken(c); err != nil {
			return
		}
	} else if wsProto := c.GetHeader("Sec-WebSocket-Protocol"); wsProto != "" {
		if wsTok, code, altErr := CheckTokenValue(s, wsProto); altErr != nil {
			log.Warn().Msg(fmt.Sprintf("bad token (%s) in Sec-WebSocket-Protocol: %+v", wsProto, altErr.Error()))
			c.AbortWithStatusJSON(code, gin.H{"error": altErr.Error()})
			return
		} else {
			tok = wsTok
			subprotos = append(subprotos, wsProto)
		}
	}
	if tok == nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "no token provided"})
		return
	} else {
		log.Debug().Msg(fmt.Sprintf("valid token from websocket"))
	}

	ech := make(chan *subscription.Event)
	if err := s.sm.UpdateChannel(idStr, ech, &tok.Token); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// See if client wants to reuse subscription upon websocket close.
	deleteOnClose := true
	if tokenString := c.GetHeader("X-Api-Delete-On-Close"); tokenString == "false" {
		c.Set("zms.deleteOnClose", false)
		deleteOnClose = false
	} else {
		c.Set("zms.deleteOnClose", true)
	}

	defer func() {
		if deleteOnClose {
			s.sm.Unsubscribe(idStr)
		} else {
			log.Debug().Msg(fmt.Sprintf("retaining subscription %s", idStr))
			s.sm.UpdateChannel(idStr, nil, &tok.Token)
		}
	}()

	wsUpgrader := websocket.Upgrader{
		ReadBufferSize: 0,
		WriteBufferSize: 16384,
		Subprotocols: subprotos,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}

	var conn *websocket.Conn
	if conn, err = wsUpgrader.Upgrade(c.Writer, c.Request, nil); err != nil {
		log.Debug().Msg(fmt.Sprintf("error upgrading websocket conn: %+v", err.Error()))
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Handle client disconnect.
	cch := make(chan interface{})
	go func() {
		for {
			_, _, err := conn.ReadMessage()
			if err != nil {
				cch <- nil
				conn.Close()
				return
			}
		}
	}()

	// Read events from SubscriptionManager, and client read errors or
	// client disconnection.
	for {
		select {
		case e := <-ech:
			if body, jErr := json.Marshal(e); jErr != nil {
				log.Error().Err(jErr).Msg(fmt.Sprintf("error marshaling event (%+v): %s", e, err.Error()))
				continue
			} else {
				if err = conn.WriteMessage(websocket.TextMessage, body); err != nil {
					return
				}
			}
		case <-cch:
			// NB: the deferred handler covers this case.
			return
		}
	}

	return
}

func (s *Server) DeleteSubscription(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("subscription_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
	} else {
		id = idParsed
	}
	idStr := id.String()

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	var sub *subscription.Subscription[*subscription.Event]
	if sub = s.sm.GetSubscription(idStr); sub == nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "not found"})
		return
	}

	// Check policy.
	if match, _ := s.CheckPolicyMiddleware(tok, nil, nil, policy.AdminOnlyPolicy); !match {
		if sub.Token == nil || *sub.Token != tok.Token {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
			return
		}
	}

	if err := s.sm.Unsubscribe(idStr); err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "not found"})
		return
	}

	c.Status(http.StatusAccepted)

	return
}
