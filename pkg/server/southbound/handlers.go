// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package southbound

import (
	"fmt"

	"github.com/rs/zerolog/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/peer"
	"google.golang.org/grpc/status"

	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/subscription"

	alarm "gitlab.flux.utah.edu/openzms/zms-api/go/zms/alarm/v1"

	"gitlab.flux.utah.edu/openzms/zms-alarm/pkg/store"
)

func EventToProto(e *subscription.Event, include bool, elaborate bool) (*alarm.Event, error) {
	ep := &alarm.Event{
		Header: e.Header.ToProto(),
	}

	if include && e.Object != nil {
		switch v := e.Object.(type) {
		case *store.ConstraintChange:
			x := new(alarm.ConstraintChange)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &alarm.Event_ConstraintChange{ConstraintChange: x}
		case *store.GrantChange:
			x := new(alarm.GrantChange)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &alarm.Event_GrantChange{GrantChange: x}
		default:
			return nil, fmt.Errorf("unknown event object type %T", v)
		}
	}

	log.Debug().Msg(fmt.Sprintf("EventToProto: %+v", ep))

	return ep, nil
}

func (s *Server) Subscribe(req *alarm.SubscribeRequest, stream alarm.Alarm_SubscribeServer) error {
	if req == nil || req.Header == nil || req.Header.ReqId == nil {
		return status.Errorf(codes.InvalidArgument, "Invalid SubscribeRequest")
	}
	if err := req.Validate(); err != nil {
		return status.Errorf(codes.InvalidArgument, err.Error())
	}

	var filters []subscription.EventFilter
	if req.Filters != nil && len(req.Filters) > 0 {
		filters = make([]subscription.EventFilter, 0, len(req.Filters))
		for _, f := range req.Filters {
			filters = append(filters, subscription.EventFilterFromProto(f))
		}
	}
	ch := make(chan *subscription.Event)
	sid := *req.Header.ReqId
	endpoint := ""
	if p, ok := peer.FromContext(stream.Context()); ok {
		endpoint = p.Addr.String()
	}
	if err := s.sm.Subscribe(sid, filters, ch, endpoint, subscription.Rpc, nil); err != nil {
		return status.Errorf(codes.Internal, err.Error())
	}

	// Handle write panics as well as the regular return cases.
	defer func() {
		if true || recover() != nil {
			s.sm.Unsubscribe(sid)
		}
	}()

	include := req.Include == nil || *req.Include
	elaborate := req.Header.Elaborate == nil || *req.Header.Elaborate

	// Watch for events and client connection error conditions
	// (e.g. disconnect).
	for {
		select {
		case e := <-ch:
			if ep, err := EventToProto(e, include, elaborate); err != nil {
				log.Error().Err(err).Msg("failed to marshal event to RPC")
				continue
			} else {
				events := make([]*alarm.Event, 0, 1)
				events = append(events, ep)
				resp := &alarm.SubscribeResponse{
					Header: &alarm.ResponseHeader{
						ReqId: req.Header.ReqId,
					},
					Events: events,
				}
				if err := stream.Send(resp); err != nil {
					break
				}
			}
		case <-stream.Context().Done():
			log.Debug().Msg(fmt.Sprintf("stream closed unexpectedly (%s)", sid))
			return nil
		}
	}

	return nil
}
